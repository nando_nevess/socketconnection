#include "utility.h"

namespace wpp {
	namespace net {
		namespace socket {
			class socket_client : public std::enable_shared_from_this<socket_client> {
				std::shared_ptr<boost::asio::ip::tcp::socket> _my_socket;
				boost::asio::io_service& _io_service;

				char recv_buffer[100000];
				std::string send_buffer;

				wpp::thread::sync::mutex mtx_access;

				bool is_sending;
				bool is_recving;
				bool is_close ;

				std::string current_server_ip;
				uint16_t current_server_port;
				uint32_t client_from_server;

				bool connection_error;
				bool is_connecting;
				bool is_connect;
				bool first_time;

				std::function<void()> callback_first_packet;
				std::function<void(char*, uint16_t)> callback_process_first_packet;
				std::function<void(char*, uint16_t, uint8_t)> callback_process_message;
				std::function<std::string(std::string)> callback_compress;

				std::map<uint8_t, std::string> buffer_to_send_list;
			public:
				socket_client(boost::asio::io_service& _service) : _io_service(_service) {
					is_sending = false;
					is_recving = false;
					is_close = false;

					current_server_ip = "";
					current_server_port = 0;
					client_from_server = 0;

					connection_error = false;
					is_connecting = false;
					is_connect = false;
					first_time = true;

					std::thread([&]() { thread_send_buffer(); }).detach();
				}

				bool get_connection_error() {
					return this->connection_error;
				}
				bool get_is_connect() {
					return is_connect;
				}

				void set_client_from_server(uint32_t in) {
					client_from_server = in;
				}

				bool wait_buffer_send(uint32_t timeout) {
					wpp::time_ns::time_chronometer timer;
					while (timer.elapsed_milliseconds() < timeout) {
						if (!is_connect)
							return false;

						if (!is_sending)
							return true;
						
						std::this_thread::sleep_for(std::chrono::milliseconds(10));
					}
					return false;
				}
				bool wait_firts_packet(uint32_t timeout) {
					wpp::time_ns::time_chronometer timer;
					while (timer.elapsed_milliseconds() < timeout) {
						if (!first_time)
							return true;

						std::this_thread::sleep_for(std::chrono::milliseconds(10));
					}
					return false;
				}

				void set_callback_process_first_packet(std::function<void(char*, uint16_t)> _callback) {
					this->callback_process_first_packet = _callback;
				}
				void set_callback_process_message(std::function<void(char*, uint16_t,uint8_t)> _callback) {
					this->callback_process_message = _callback;
				}
				void set_callback_first_packet(std::function<void()> _callback) {
					this->callback_first_packet = _callback;
				}

				void thread_send_buffer() {
					while (true) {
						Sleep(10);
						
						if (!is_connect || connection_error)
							continue;

						for (auto current_buffer_send : buffer_to_send_list) {
							wait_firts_packet(5 * 1000);

							if (!is_connect || connection_error)
								break;

							if (wpp::string::remove_space(current_buffer_send.second) == "")
								continue;						

							mtx_access.lock();
							std::string& current_buffer_string = current_buffer_send.second;
							uint8_t current_request_Id = current_buffer_send.first;
							uint32_t rett_size_client_from_server = client_from_server;
							mtx_access.unlock();

							bool first = true;
							while (true) {
								if (!is_connect || connection_error)
									break;

								if (wpp::string::remove_space(current_buffer_string) == "" || !current_buffer_string.size()) {
									this->buffer_to_send_list[current_request_Id] = "";
									break;
								}

								std::string buffer_to_send((char*)&current_request_Id, sizeof(uint8_t));

								if (!first)
									rett_size_client_from_server = client_from_server;
								else if (rett_size_client_from_server > 0)
									rett_size_client_from_server += 0;
																
								if (current_buffer_string.size() <= rett_size_client_from_server || rett_size_client_from_server == 0) {
									buffer_to_send.append(current_buffer_string.data(), current_buffer_string.size());
									current_buffer_string.clear();
								}
								else {
									buffer_to_send.append(current_buffer_string.begin(), current_buffer_string.begin() + rett_size_client_from_server);
									current_buffer_string.erase(current_buffer_string.begin(), current_buffer_string.begin() + rett_size_client_from_server);
								}

								std::shared_ptr<std::string> buffer_temp(new std::string(buffer_to_send.data(), buffer_to_send.size()));

								first = false;
								std::cout << "\n SEND: " << buffer_to_send;

								add_send_data(buffer_temp);
								wait_buffer_send(5 * 1000);
							}
						}
					}
				}

				void add_send_buffer_to_list(std::string data, uint8_t request_id) {
					if (!is_connect || connection_error)
						return;

					buffer_to_send_list[request_id] = data;
					check_send_data();
				}
				void add_send_data(std::shared_ptr<std::string> data) {
					if (is_sending)
						send_buffer.clear();

					if (send_buffer.size())
						send_buffer.clear();

					send_buffer.append(data->data(), data->size());
					check_send_data();
				}

				void check_send_data() {
					if (!_my_socket)
						return;

					if (is_sending || !send_buffer.size() || !_my_socket->is_open())
						return;

					std::shared_ptr<std::string> _buffer(new std::string(send_buffer.data(), send_buffer.size()));

					//std::cout << "\n SEND_END: " << send_buffer;

					is_sending = true;
					_my_socket->async_send(boost::asio::buffer(_buffer->data(), _buffer->size()),
						boost::bind(&socket_client::on_send, shared_from_this(), _my_socket,
							boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, _buffer));
				}
				void check_set_recv() {
					if (is_recving || !_my_socket->is_open())
						return;

					is_recving = true;

					_my_socket->async_receive(boost::asio::buffer(recv_buffer, sizeof(recv_buffer)),
						boost::bind(&socket_client::on_recv, shared_from_this(), _my_socket,
							boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
				}

				void set_connect(std::string ip, uint16_t port) {
					wpp::thread::sync::lock_guard <wpp::thread::sync::mutex>_guard(mtx_access);

					if (!connection_error && current_server_ip == ip && current_server_port == port)
						return;
										
					current_server_ip = ip;
					current_server_port = port;
					is_close = false;

					if (_my_socket && _my_socket->is_open())
						_my_socket->close();
					
					connect(true);
				}

				bool create_socket() {
					try {
						_my_socket = std::shared_ptr<boost::asio::ip::tcp::socket>(new boost::asio::ip::tcp::socket(_io_service, boost::asio::ip::tcp::v4()));
					}
					catch (...) {
						return false;
					}
					return true;
				}

				void connect(bool lock) {
					if (is_connecting)
						return;

					if (is_close)
						return;

					if (lock)
						mtx_access.lock();

					std::string server_ip = current_server_ip;
					uint16_t server_port = current_server_port;

					try {
						if (!_my_socket) {
							_my_socket = std::shared_ptr<boost::asio::ip::tcp::socket>(new boost::asio::ip::tcp::socket(_io_service));
						}
						else {
							if (_my_socket->is_open())
								_my_socket->close();

							_my_socket = std::shared_ptr<boost::asio::ip::tcp::socket>(new boost::asio::ip::tcp::socket(_io_service));
						}

						boost::asio::ip::address_v4::from_string(server_ip);
					}
					catch (std::exception& ex) {
						std::cout << "\n EX " << ex.what();
						if (lock)
							mtx_access.unlock();

						return;
					}

					is_connecting = true;
					connection_error = false;

					_my_socket->async_connect(boost::asio::ip::tcp::endpoint(
						boost::asio::ip::address_v4::from_string(server_ip), server_port),
						boost::bind(&socket_client::on_connect, shared_from_this(), _my_socket, boost::asio::placeholders::error));

					if (lock)
						mtx_access.unlock();
				}
				void close() {
					if (is_close)
						return;

					if (_my_socket && _my_socket->is_open())
						_my_socket->close();
				}

				void on_connect(std::shared_ptr<boost::asio::ip::tcp::socket> socket_holder, const boost::system::error_code& error) {
					is_connecting = false;
					is_connect = true;

					if (error) {
						std::cout << "\n error: " << error << " - message: " << error.message();
						if (socket_holder == _my_socket) {
							is_connect = false;
							connection_error = true;
							connect(true);
						}
						return;
					}

					mtx_access.lock();
					if (current_server_ip != socket_holder->remote_endpoint().address().to_string() || socket_holder->remote_endpoint().port() != current_server_port) {
						if (socket_holder == _my_socket) {
							is_connect = false;
							connection_error = true;
							connect(true);
						}
						mtx_access.unlock();
						return;
					}
					mtx_access.unlock();

					std::cout << "\n SOCKET - CONECTADO COM SUCESSO";

					buffer_to_send_list.clear();

					if (callback_first_packet)
						callback_first_packet();

					check_set_recv();
				}
				void on_recv(std::shared_ptr<boost::asio::ip::tcp::socket> socket_holder, const boost::system::error_code& error, const size_t bytes_transferred) {
					is_recving = false;

					if (error && error.value() != 2) {
						std::cout << "\n error: " << error << " - message: " << error.message();
						if (_my_socket != socket_holder)
							return;

						connection_error = true;
						connect(true);
						return;
					}

					if (bytes_transferred > 0) {
						callback_process_message(recv_buffer, bytes_transferred, 0);
					}

					check_set_recv();
				}
				void on_send(std::shared_ptr<boost::asio::ip::tcp::socket> socket_holder, const boost::system::error_code& error, const size_t bytes_transferred, std::shared_ptr<std::string> _data_holder) {
					send_buffer.clear();
					is_sending = false;

					if (error) {
						std::cout << "\n ERROR " << error.message();

						if (_my_socket != socket_holder)
							return;

						connection_error = true;
						connect(true);
						return;
					}

					check_send_data();
				}
			};
		}
	}
}