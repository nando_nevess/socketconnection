#include <wpp\libs\OpenSSL-Win32\include\openssl\evp.h>
#include <wpp\libs\OpenSSL-Win32\include\openssl\pem.h>
#include <wpp\libs\OpenSSL-Win32\include\openssl\aes.h>
#include <wpp\libs\OpenSSL-Win32\include\openssl\err.h>
#include <wpp\libs\OpenSSL-Win32\include\openssl\rand.h>

#pragma comment(lib,"libeay32.lib")
#pragma comment(lib,"ssleay32.lib")

#include <stdio.h>
#include <iostream>
#include <string.h>

#define USE_PBKDF

#define RSA_KEYLEN 2048
#define AES_KEYLEN 256
#define AES_ROUNDS 6

#define PSEUDO_CLIENT

#define SUCCESS 0
#define FAILURE -1

#define KEY_SERVER_PRI 0
#define KEY_SERVER_PUB 1
#define KEY_CLIENT_PUB 2
#define KEY_AES        3
#define KEY_AES_IV     4

namespace wpp {
	namespace net {
		namespace socket {
			static const unsigned int KEY_SIZE = 32;
			static const unsigned int BLOCK_SIZE = 16;

			template <typename T>
			struct zallocator {
			public:
				typedef T value_type;
				typedef value_type* pointer;
				typedef const value_type* const_pointer;
				typedef value_type& reference;
				typedef const value_type& const_reference;
				typedef std::size_t size_type;
				typedef std::ptrdiff_t difference_type;

				pointer address(reference v) const { return &v; }
				const_pointer address(const_reference v) const { return &v; }

				pointer allocate(size_type n, const void* hint = 0) {
					//auto temp = std::numeric_limits<size_type>::max() / sizeof(T);
					//if (n > temp)
						//throw std::bad_alloc();

					return static_cast<pointer> (::operator new (n * sizeof(value_type)));
				}

				void deallocate(pointer p, size_type n) {
					OPENSSL_cleanse(p, n * sizeof(T));
					::operator delete(p);
				}

				/*size_type max_size() const {
					//return std::numeric_limits<size_type>::max() / sizeof(T);
					return size_type;
				}*/

				template<typename U>
				struct rebind
				{
					typedef zallocator<U> other;
				};

				void construct(pointer ptr, const T& val) {
					new (static_cast<T*>(ptr)) T(val);
				}

				void destroy(pointer ptr) {
					static_cast<T*>(ptr)->~T();
				}

#if __cpluplus >= 201103L
				template<typename U, typename... Args>
				void construct(U* ptr, Args&&  ... args) {
					::new (static_cast<void*> (ptr)) U(std::forward<Args>(args)...);
				}

				template<typename U>
				void destroy(U* ptr) {
					ptr->~U();
				}
#endif
			};

			typedef unsigned char byte;
			typedef std::basic_string<char, std::char_traits<char>, zallocator<char>> secure_string;
			using EVP_CIPHER_CTX_free_ptr = std::unique_ptr<EVP_CIPHER_CTX, decltype(&::EVP_CIPHER_CTX_free)>;

			class socket_crypto {
			public:
				socket_crypto() {
					localKeypair = NULL;
					remotePublicKey = NULL;

#ifdef PSEUDO_CLIENT
					generateRsaKeypair(&remotePublicKey);
#endif

					init();
				}
				socket_crypto(unsigned char *remotePublicKey, size_t remotePublicKeyLength) {
					localKeypair = NULL;
					this->remotePublicKey = NULL;

					setRemotePublicKey(remotePublicKey, remotePublicKeyLength);
					init();
				}
				~socket_crypto() {
					EVP_PKEY_free(remotePublicKey);

					EVP_CIPHER_CTX_free(rsaEncryptContext);
					EVP_CIPHER_CTX_free(aesEncryptContext);

					EVP_CIPHER_CTX_free(rsaDecryptContext);
					EVP_CIPHER_CTX_free(aesDecryptContext);

					free(aesKey);
					free(aesIv);
				}

				int rsaEncrypt(const unsigned char *message, size_t messageLength, unsigned char **encryptedMessage, unsigned char **encryptedKey, size_t *encryptedKeyLength, unsigned char **iv, size_t *ivLength) {
					// Allocate memory for everything
					size_t encryptedMessageLength = 0;
					size_t blockLength = 0;

					*encryptedKey = (unsigned char*)malloc(EVP_PKEY_size(remotePublicKey));
					*iv = (unsigned char*)malloc(EVP_MAX_IV_LENGTH);
					*ivLength = EVP_MAX_IV_LENGTH;

					if (*encryptedKey == NULL || *iv == NULL)
						return FAILURE;

					*encryptedMessage = (unsigned char*)malloc(messageLength + EVP_MAX_IV_LENGTH);
					if (encryptedMessage == NULL)
						return FAILURE;

					// Encrypt it!
					if (!EVP_SealInit(rsaEncryptContext, EVP_aes_256_cbc(), encryptedKey, (int*)encryptedKeyLength, *iv, &remotePublicKey, 1))
						return FAILURE;

					if (!EVP_SealUpdate(rsaEncryptContext, *encryptedMessage + encryptedMessageLength, (int*)&blockLength, (const unsigned char*)message, (int)messageLength))
						return FAILURE;

					encryptedMessageLength += blockLength;

					if (!EVP_SealFinal(rsaEncryptContext, *encryptedMessage + encryptedMessageLength, (int*)&blockLength))
						return FAILURE;

					encryptedMessageLength += blockLength;

					return (int)encryptedMessageLength;
				}
				int rsaDecrypt(unsigned char *encryptedMessage, size_t encryptedMessageLength, unsigned char *encryptedKey, size_t encryptedKeyLength, unsigned char *iv, size_t ivLength, unsigned char **decryptedMessage) {
					// Allocate memory for everything
					size_t decryptedMessageLength = 0;
					size_t blockLength = 0;

					*decryptedMessage = (unsigned char*)malloc(encryptedMessageLength + ivLength);
					if (*decryptedMessage == NULL)
						return FAILURE;

#ifdef PSEUDO_CLIENT
					EVP_PKEY *key = remotePublicKey;
#else
					EVP_PKEY *key = localKeypair;
#endif

					// Decrypt it!
					if (!EVP_OpenInit(rsaDecryptContext, EVP_aes_256_cbc(), encryptedKey, encryptedKeyLength, iv, key))
						return FAILURE;

					if (!EVP_OpenUpdate(rsaDecryptContext, (unsigned char*)*decryptedMessage + decryptedMessageLength, (int*)&blockLength, encryptedMessage, (int)encryptedMessageLength))
						return FAILURE;

					decryptedMessageLength += blockLength;

					if (!EVP_OpenFinal(rsaDecryptContext, (unsigned char*)*decryptedMessage + decryptedMessageLength, (int*)&blockLength))
						return FAILURE;

					decryptedMessageLength += blockLength;

					return (int)decryptedMessageLength;
				}

				RSA * createRSA(unsigned char * key, int _public) {
					RSA *rsa = NULL;
					BIO *keybio;
					keybio = BIO_new_mem_buf(key, -1);
					if (keybio == NULL) {
						printf("Failed to create key BIO");
						return 0;
					}

					if (_public)
						rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa, NULL, NULL);
					else
						rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, NULL, NULL);

					if (rsa == NULL)
						printf("Failed to create RSA");

					return rsa;
				}

				int aesEncrypt(const unsigned char *message, size_t messageLength, unsigned char **encryptedMessage) {
					// Allocate memory for everything
					size_t blockLength = 0;
					size_t encryptedMessageLength = 0;

					*encryptedMessage = (unsigned char*)malloc(messageLength + AES_BLOCK_SIZE);
					if (encryptedMessage == NULL)
						return FAILURE;

					// Encrypt it!
					if (!EVP_EncryptInit_ex(aesEncryptContext, EVP_aes_256_cbc(), NULL, aesKey, aesIv))
						return FAILURE;

					if (!EVP_EncryptUpdate(aesEncryptContext, *encryptedMessage, (int*)&blockLength, (unsigned char*)message, messageLength))
						return FAILURE;

					encryptedMessageLength += blockLength;

					if (!EVP_EncryptFinal_ex(aesEncryptContext, *encryptedMessage + encryptedMessageLength, (int*)&blockLength))
						return FAILURE;

					return encryptedMessageLength + blockLength;
				}
				int aesDecrypt(unsigned char *encryptedMessage, size_t encryptedMessageLength, unsigned char* aesKey_,unsigned char* aesIv_, unsigned char **decryptedMessage) {
					// Allocate memory for everything
					size_t decryptedMessageLength = 0;
					size_t blockLength = 0;

					*decryptedMessage = (unsigned char*)malloc(encryptedMessageLength);
					if (*decryptedMessage == NULL)
						return FAILURE;

					// Decrypt it!
					if (!EVP_DecryptInit_ex(aesDecryptContext, EVP_aes_256_cbc(), NULL, aesKey_, NULL))
						return FAILURE;

					if (!EVP_DecryptUpdate(aesDecryptContext, (unsigned char*)*decryptedMessage, (int*)&blockLength, encryptedMessage, (int)encryptedMessageLength))
						return FAILURE;

					decryptedMessageLength += blockLength;

					if (!EVP_DecryptFinal_ex(aesDecryptContext, (unsigned char*)*decryptedMessage + decryptedMessageLength, (int*)&blockLength))
						return FAILURE;

					decryptedMessageLength += blockLength;

					return (int)decryptedMessageLength;
				}

				int aesEncrypt2(unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *ciphertext)
				{
					EVP_CIPHER_CTX *ctx;

					int len;

					int ciphertext_len;

					/* Create and initialise the context */
					if (!(ctx = EVP_CIPHER_CTX_new())) return FAILURE;

					/* Initialise the encryption operation. IMPORTANT - ensure you use a key
					* In this example we are using 256 bit AES (i.e. a 256 bit key).
					*/
					if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ecb(), NULL, key, NULL))
						return FAILURE;

					/* Provide the message to be encrypted, and obtain the encrypted output.
					* EVP_EncryptUpdate can be called multiple times if necessary
					*/
					if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
						return FAILURE;
					ciphertext_len = len;

					/* Finalise the encryption. Further ciphertext bytes may be written at
					* this stage.
					*/
					if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))  return FAILURE;
					ciphertext_len += len;

					/* Clean up */
					EVP_CIPHER_CTX_free(ctx);

					return ciphertext_len;
				}

				int aesDecrypt2(unsigned char *ciphertext, int ciphertext_len, unsigned char *key, unsigned char *plaintext)
				{
					ERR_print_errors_fp(stderr);
					EVP_CIPHER_CTX *ctx;

					int len;

					int plaintext_len;

					/* Create and initialise the context */
					if (!(ctx = EVP_CIPHER_CTX_new())) return FAILURE;		
					//EVP_CIPHER_CTX_set_padding(ctx, 0);
					

					OpenSSL_add_all_algorithms();
					unsigned char real_key[32];
					
					int key_len = EVP_BytesToKey(EVP_aes_256_ecb(), EVP_md5(), NULL, key, 32, 1, real_key, NULL);

					/* Initialise the decryption operation. IMPORTANT - ensure you use a key
					* and IV size appropriate for your cipher
					* In this example we are using 256 bit AES (i.e. a 256 bit key). The
					* IV size for *most* modes is the same as the block size. For AES this
					* is 128 bits */
					if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_ecb(), NULL, real_key, NULL))
						return FAILURE;				

					/* Provide the message to be decrypted, and obtain the plaintext output.
					* EVP_DecryptUpdate can be called multiple times if necessary
					*/
					if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
						return FAILURE;
					plaintext_len = len;

					/* Finalise the decryption. Further plaintext bytes may be written at
					* this stage.
					*/
					auto xd = EVP_DecryptFinal_ex(ctx, plaintext + len, &len);
					if (1 != xd) {							
							return FAILURE;
					}
					plaintext_len += len;

					/* Clean up */
					EVP_CIPHER_CTX_free(ctx);

					return plaintext_len;
				}

				/*int aesDecrypt(unsigned char *encryptedMessage, size_t encryptedMessageLength, unsigned char **decryptedMessage) {
					// Allocate memory for everything
					size_t decryptedMessageLength = 0;
					size_t blockLength = 0;

					*decryptedMessage = (unsigned char*)malloc(encryptedMessageLength);
					if (*decryptedMessage == NULL)
						return FAILURE;

					// Decrypt it!
					if (!EVP_DecryptInit_ex(aesDecryptContext, EVP_aes_256_cbc(), NULL, aesKey, aesIv))
						return FAILURE;

					if (!EVP_DecryptUpdate(aesDecryptContext, (unsigned char*)*decryptedMessage, (int*)&blockLength, encryptedMessage, (int)encryptedMessageLength))
						return FAILURE;

					decryptedMessageLength += blockLength;

					if (!EVP_DecryptFinal_ex(aesDecryptContext, (unsigned char*)*decryptedMessage + decryptedMessageLength, (int*)&blockLength))
						return FAILURE;

					decryptedMessageLength += blockLength;

					return (int)decryptedMessageLength;
				}*/




				void init_main(unsigned char* key,unsigned char* encrypt_packet) {
					// Load the necessary cipher
					EVP_add_cipher(EVP_aes_256_cbc());

					// plaintext, ciphertext, recovered text
					secure_string ptext = secure_string((const char*)encrypt_packet);
					secure_string ctext, rtext;

					//byte key[KEY_SIZE];
					byte iv[BLOCK_SIZE];

					gen_params(key, iv);

					//aes_encrypt(key, iv, ptext, ctext);
					aes_decrypt(key, iv, ptext, rtext);

					OPENSSL_cleanse(key, KEY_SIZE);
					OPENSSL_cleanse(iv, BLOCK_SIZE);

					std::cout << "\n";
					std::cout << "Original message:\n" << ptext << std::endl;
					std::cout << "Recovered message:\n" << rtext << std::endl;
				}

				void gen_params(byte key[KEY_SIZE], byte iv[BLOCK_SIZE]){
					//int rc = RAND_bytes(key, KEY_SIZE);
					//if (rc != 1)
						//throw std::runtime_error("RAND_bytes key failed");

					int rc = RAND_bytes(iv, BLOCK_SIZE);
					if (rc != 1)
						throw std::runtime_error("RAND_bytes for iv failed");
				}

				void aes_encrypt(const byte key[KEY_SIZE], const byte iv[BLOCK_SIZE], const secure_string& ptext, secure_string& ctext){
					EVP_CIPHER_CTX_free_ptr ctx(EVP_CIPHER_CTX_new(), ::EVP_CIPHER_CTX_free);
					int rc = EVP_EncryptInit_ex(ctx.get(), EVP_aes_256_cbc(), NULL, key, iv);
					if (rc != 1)
						throw std::runtime_error("EVP_EncryptInit_ex failed");

					// Recovered text expands upto BLOCK_SIZE
					ctext.resize(ptext.size() + BLOCK_SIZE);
					int out_len1 = (int)ctext.size();

					rc = EVP_EncryptUpdate(ctx.get(), (byte*)&ctext[0], &out_len1, (const byte*)&ptext[0], (int)ptext.size());
					if (rc != 1)
						throw std::runtime_error("EVP_EncryptUpdate failed");

					int out_len2 = (int)ctext.size() - out_len1;
					rc = EVP_EncryptFinal_ex(ctx.get(), (byte*)&ctext[0] + out_len1, &out_len2);
					if (rc != 1)
						throw std::runtime_error("EVP_EncryptFinal_ex failed");

					// Set cipher text size now that we know it
					ctext.resize(out_len1 + out_len2);
				}
				void aes_decrypt(const byte key[KEY_SIZE], const byte iv[BLOCK_SIZE], const secure_string& ctext, secure_string& rtext){
					EVP_CIPHER_CTX_free_ptr ctx(EVP_CIPHER_CTX_new(), ::EVP_CIPHER_CTX_free);
					int rc = EVP_DecryptInit_ex(ctx.get(), EVP_aes_256_cbc(), NULL, key, iv);
					if (rc != 1)
						throw std::runtime_error("EVP_DecryptInit_ex failed");

					// Recovered text contracts upto BLOCK_SIZE
					rtext.resize(ctext.size());
					int out_len1 = (int)rtext.size();

					rc = EVP_DecryptUpdate(ctx.get(), (byte*)&rtext[0], &out_len1, (const byte*)&ctext[0], (int)ctext.size());
					if (rc != 1)
						throw std::runtime_error("EVP_DecryptUpdate failed");

					int out_len2 = (int)rtext.size() - out_len1;
					rc = EVP_DecryptFinal_ex(ctx.get(), (byte*)&rtext[0] + out_len1, &out_len2);
					if (rc != 1)
						throw std::runtime_error("EVP_DecryptFinal_ex failed");

					// Set recovered text size now that we know it
					rtext.resize(out_len1 + out_len2);
				}


				int aes_init(unsigned char *key_data, int key_data_len, unsigned char *salt, EVP_CIPHER_CTX *e_ctx, EVP_CIPHER_CTX *d_ctx) {
					int i, nrounds = 5;
					unsigned char key[32], iv[32];

					/*
					* Gen key & IV for AES 256 CBC mode. A SHA1 digest is used to hash the supplied key material.
					* nrounds is the number of times the we hash the material. More rounds are more secure but
					* slower.
					*/
					i = EVP_BytesToKey(EVP_aes_256_cbc(), EVP_sha1(), salt, key_data, key_data_len, nrounds, key, iv);
					if (i != 32) {
						printf("Key size is %d bits - should be 256 bits\n", i);
						return -1;
					}

					EVP_CIPHER_CTX_init(e_ctx);
					EVP_EncryptInit_ex(e_ctx, EVP_aes_256_cbc(), NULL, key, iv);
					EVP_CIPHER_CTX_init(d_ctx);
					EVP_DecryptInit_ex(d_ctx, EVP_aes_256_cbc(), NULL, key, iv);

					return 0;
				}
				unsigned char *aes_decrypt(EVP_CIPHER_CTX *e, unsigned char *ciphertext, int *len) {
					/* plaintext will always be equal to or lesser than length of ciphertext*/
					int p_len = *len, f_len = 0;
					unsigned char *plaintext = (unsigned char*)malloc(p_len);

					EVP_DecryptInit_ex(e, NULL, NULL, NULL, NULL);
					EVP_DecryptUpdate(e, plaintext, &p_len, ciphertext, *len);
					EVP_DecryptFinal_ex(e, plaintext + p_len, &f_len);

					*len = p_len + f_len;
					return plaintext;
				}
				unsigned char *aes_encrypt(EVP_CIPHER_CTX *e, unsigned char *plaintext, int *len) {
					/* max ciphertext len for a n bytes of plaintext is n + AES_BLOCK_SIZE -1 bytes */
					int c_len = *len + AES_BLOCK_SIZE, f_len = 0;
					unsigned char *ciphertext = (unsigned char*)malloc(c_len);

					/* allows reusing of 'e' for multiple encryption cycles */
					EVP_EncryptInit_ex(e, NULL, NULL, NULL, NULL);

					/* update ciphertext, c_len is filled with the length of ciphertext generated,
					*len is the size of plaintext in bytes */
					EVP_EncryptUpdate(e, ciphertext, &c_len, plaintext, *len);

					/* update ciphertext with the final remaining bytes */
					EVP_EncryptFinal_ex(e, ciphertext + c_len, &f_len);

					*len = c_len + f_len;
					return ciphertext;
				}

				int getRemotePublicKey(unsigned char **publicKey) {
					BIO *bio = BIO_new(BIO_s_mem());
					PEM_write_bio_PUBKEY(bio, remotePublicKey);
					return bioToString(bio, publicKey);
				}
				int setRemotePublicKey(unsigned char *publicKey, size_t publicKeyLength) {
					BIO *bio = BIO_new(BIO_s_mem());
					if (BIO_write(bio, publicKey, publicKeyLength) != (int)publicKeyLength)
						return FAILURE;

					PEM_read_bio_PUBKEY(bio, &remotePublicKey, NULL, NULL);
					BIO_free_all(bio);

					return SUCCESS;
				}

				int getLocalPublicKey(unsigned char **publicKey) {
					BIO *bio = BIO_new(BIO_s_mem());
					PEM_write_bio_PUBKEY(bio, localKeypair);
					return bioToString(bio, publicKey);
				}
				int getLocalPrivateKey(unsigned char **privateKey) {
					BIO *bio = BIO_new(BIO_s_mem());
					PEM_write_bio_PrivateKey(bio, localKeypair, NULL, NULL, 0, 0, NULL);
					return bioToString(bio, privateKey);
				}

				int getAesKey(unsigned char **aesKey) {
					*aesKey = this->aesKey;
					return AES_KEYLEN / 8;
				}
				int setAesKey(unsigned char *aesKey, size_t aesKeyLength) {
					// Ensure the new key is the proper size
					if ((int)aesKeyLength != AES_KEYLEN / 8)
						return FAILURE;

					memcpy(this->aesKey, aesKey, AES_KEYLEN / 8);

					return SUCCESS;
				}

				int getAesIv(unsigned char **aesIv) {
					*aesIv = this->aesIv;
					return AES_KEYLEN / 8;
				}
				int setAesIv(unsigned char *aesIv, size_t aesIvLength) {
					// Ensure the new IV is the proper size
					if ((int)aesIvLength != AES_KEYLEN / 8)
						return FAILURE;

					memcpy(this->aesIv, aesIv, AES_KEYLEN / 8);

					return SUCCESS;
				}

				int writeKeyToFile(FILE *file, int key) {
					switch (key) {
					case KEY_SERVER_PRI:
						std::cout << "\n PRIVATE_KEY: " << localKeypair;
						//if(!PEM_write_PrivateKey(file, localKeypair, NULL, NULL, 0, 0, NULL)) {
						//return FAILURE;
						//}
						break;

					case KEY_SERVER_PUB:
						std::cout << "\n PUBLIC_KEY_SERVER: " << localKeypair;
						//if(!PEM_write_PUBKEY(file, localKeypair)) {
						//return FAILURE;
						//}
						break;

					case KEY_CLIENT_PUB:
						std::cout << "\n PUBLIC_KEY_CLIENT: " << remotePublicKey;
						//if(!PEM_write_PUBKEY(file, remotePublicKey)) {
						//return FAILURE;
						//}
						break;

					case KEY_AES:
						std::cout << "\n AES_KEYLEN: " << aesKey;
						//fwrite(aesKey, 1, AES_KEYLEN, file);
						break;

					case KEY_AES_IV:
						std::cout << "\n KEY_AES_IV: " << aesIv;
						//fwrite(aesIv, 1, AES_KEYLEN, file);
						break;

					default:
						return FAILURE;
					}

					return SUCCESS;
				}
			private:
				EVP_PKEY *localKeypair;
				EVP_PKEY *remotePublicKey;

				EVP_CIPHER_CTX *rsaEncryptContext;
				EVP_CIPHER_CTX *aesEncryptContext;

				EVP_CIPHER_CTX *rsaDecryptContext;
				EVP_CIPHER_CTX *aesDecryptContext;

				unsigned char *aesKey;
				unsigned char *aesIv;

				int init() {
					// Initalize contexts
					rsaEncryptContext = EVP_CIPHER_CTX_new();
					aesEncryptContext = EVP_CIPHER_CTX_new();

					rsaDecryptContext = EVP_CIPHER_CTX_new();
					aesDecryptContext = EVP_CIPHER_CTX_new();

					// Check if any of the contexts initializations failed
					if (rsaEncryptContext == NULL || aesEncryptContext == NULL || rsaDecryptContext == NULL || aesDecryptContext == NULL)
						return FAILURE;

					// Generate RSA and AES keys
					//generateRsaKeypair(&localKeypair);
					generateAesKey(&aesKey, &aesIv);

					return SUCCESS;
			}
				int generateRsaKeypair(EVP_PKEY **keypair) {
					EVP_PKEY_CTX *context = EVP_PKEY_CTX_new_id(EVP_PKEY_RSA, NULL);
					if (EVP_PKEY_keygen_init(context) <= 0)
						return FAILURE;

					if (EVP_PKEY_CTX_set_rsa_keygen_bits(context, RSA_KEYLEN) <= 0)
						return FAILURE;

					if (EVP_PKEY_keygen(context, keypair) <= 0)
						return FAILURE;

					EVP_PKEY_CTX_free(context);
					return SUCCESS;
				}
				int generateAesKey(unsigned char **aesKey, unsigned char **aesIv) {
					*aesKey = (unsigned char*)malloc(AES_KEYLEN / 8);
					*aesIv = (unsigned char*)malloc(AES_KEYLEN / 8);

					if (aesKey == NULL || aesIv == NULL)
						return FAILURE;

					// For the AES key we have the option of using a PBKDF or just using straight random
					// data for the key and IV. Depending on your use case, you will want to pick one or another.
#ifdef USE_PBKDF
					unsigned char *aesPass = (unsigned char*)malloc(AES_KEYLEN / 8);
					unsigned char *aesSalt = (unsigned char*)malloc(8);

					if (aesPass == NULL || aesSalt == NULL)
						return FAILURE;

					// Get some random data to use as the AES pass and salt
					if (RAND_bytes(aesPass, AES_KEYLEN / 8) == 0)
						return FAILURE;

					if (RAND_bytes(aesSalt, 8) == 0)
						return FAILURE;

					if (EVP_BytesToKey(EVP_aes_256_cbc(), EVP_sha256(), aesSalt, aesPass, AES_KEYLEN / 8, AES_ROUNDS, *aesKey, *aesIv) == 0)
						return FAILURE;

					free(aesPass);
					free(aesSalt);
#else
					if (RAND_bytes(*aesKey, AES_KEYLEN / 8) == 0)
						return FAILURE;

					if (RAND_bytes(*aesIv, AES_KEYLEN / 8) == 0)
						return FAILURE;
#endif
					return SUCCESS;
				}
				int bioToString(BIO *bio, unsigned char **string) {
					size_t bioLength = BIO_pending(bio);
					*string = (unsigned char*)malloc(bioLength + 1);

					if (string == NULL)
						return FAILURE;

					BIO_read(bio, *string, bioLength);

					// Insert the NUL terminator
					(*string)[bioLength] = '\0';

					BIO_free_all(bio);

					return (int)bioLength;
				}
		};
	}
}
}