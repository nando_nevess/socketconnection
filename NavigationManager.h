#pragma once
#include "NavigationStream.h"
#include "ClientManager.h"
#include "Scheduler.h"
#include "json\json.h"
#include <mutex>
#include <regex>
#include <thread>

enum protocol_t {
	protocol_none,
	protocol_get_best_route,
	protocol_insert_user_path
};

class Client;

class NavigationManager : public Scheduler {
	char recv_buffer[100000];

	std::string rest_buffer = "";
	std::string response_string = "";
	
	bool string_result_on = false;
	std::mutex mtx;

	boost::asio::io_service svc;

	std::shared_ptr<Client> client_in;
public:
	Json::Value string_to_json(std::string json_data) {
		Json::Value root;
		Json::Reader reader;
		bool parsingSuccessful = reader.parse(json_data.c_str(), root);
		if (!parsingSuccessful)
			return Json::Value();

		return root;
	}
	std::string json_to_string(Json::Value json_string) {
		return json_string.toStyledString();
	}

	uint32_t string_to_uint(std::string in) {
		uint32_t temp = 0;
		try {
			temp = std::atoi(in.c_str());
		}
		catch (const std::exception&) {
			temp = 0;
		}
		return temp;

	}
	
	std::string remove_new_line(std::string in) {
		std::string _temp = in;
		_temp.erase(std::remove(_temp.begin(), _temp.end(), '\n'), _temp.end());
		return _temp;
	}
	std::string remove_space(std::string in) {
		std::string temp_ = in;
		temp_ = std::regex_replace(temp_, std::regex(" "), "");
		return temp_;
	}
	std::string remove_barra(std::string in) {
		std::string _temp = in;
		_temp.erase(std::remove(_temp.begin(), _temp.end(), '\\'), _temp.end());
		return _temp;
	}

	NavigationManager() : Scheduler(svc) {
		star_loop_client();
	}
	void star_loop_client() {
		std::thread([&]() {
			client_in = std::shared_ptr<Client>(new Client(svc));
			client_in->set_callback_process_message([&](char* message, uint16_t size) { this->process_message(message, size); });

			while (true) {
				Sleep(10);
				try {
					svc.run();
				}
				catch (std::exception& ex) {
					std::cout << "\n Runner out reason: " << ex.what();
				}
			}
		}).detach();
	}

	void connect_to_server(std::string ip, uint32_t port) {
		if (!client_in)
			return;

		this->add_task(std::bind([](std::string _ip, uint16_t _port, std::shared_ptr<Client> _cli) -> void {
			_cli->set_connect(_ip, _port);
		}, ip, port, client_in), 2000);
	}
	void close() {
		client_in->close();
	}

	std::string get_response_string() {
		mtx.lock();
		std::string in = response_string;
		string_result_on = false;
		mtx.unlock();

		return in;
	}
	void set_response_string(std::string in) {
		if (in == "")
			return;

		mtx.lock();
		this->response_string = in;
		string_result_on = true;
		mtx.unlock();
	}
	void clear_response_string() {
		mtx.lock();
		response_string.clear();
		mtx.unlock();
	}

	bool get_string_result_on() {
		return string_result_on;
	}

	std::string create_header(std::string type ,uint32_t size_result) {
		Json::Value json_header;

		json_header["type"] = type;
		json_header["size"] = std::to_string(size_result);
		
		std::string string_header = json_header.toStyledString();
		string_header = this->remove_new_line(string_header);
		string_header = this->remove_space(string_header);

		uint32_t header_size = string_header.size();
		std::string header_size_string = std::to_string(string_header.size());
		if (header_size < 10)
			header_size_string = "000" + header_size_string;
		else if(header_size < 100)
			header_size_string = "00" + header_size_string;
		else if (header_size < 1000)
			header_size_string = "0" + header_size_string;
		
		std::string retval;
		retval = header_size_string;
		retval += string_header;

		return retval;
	}

	bool send_request(protocol_t protoccol_, Json::Value json_to_send) {
		Json::Value json_to_send_;

		if (protoccol_ == protocol_get_best_route)
			json_to_send_["request_key"] = "get_best_route";
		else if (protoccol_ == protocol_insert_user_path)
			json_to_send_["request_key"] = "insert_user_path";

		json_to_send_["params"] = json_to_send;

		if (json_to_send_.isNull() || json_to_send_.empty())
			return false;

		std::string json_string_to_send = json_to_send_.toStyledString();
		json_string_to_send = remove_new_line(json_string_to_send);
		json_string_to_send = remove_space(json_string_to_send);

		std::string string_to_send = create_header("json", json_string_to_send.size());
		string_to_send = remove_new_line(string_to_send);
		string_to_send = remove_space(string_to_send);

		string_to_send += json_string_to_send;

		std::cout << "\n TEMP: \n" << string_to_send;

		std::shared_ptr<std::string> _buffer(new std::string(string_to_send.data(), string_to_send.size()));
		client_in->add_send_data(_buffer);

		return true;
	}

	void process_message(char* buffer, uint16_t dados_no_buffer) {
		if (!dados_no_buffer)
			return;

		if (buffer)
			rest_buffer.append(buffer, dados_no_buffer);

		NaviStreamReader reader((uint8_t*)rest_buffer.data(), rest_buffer.size());
		if (!reader.can_read_bytes(4))
			return;

		std::string size_header_string = reader.get_current_string(4);
		uint32_t size_header = string_to_uint(size_header_string);

		std::string header_string = reader.get_current_string(size_header);
		if (header_string == "")
			return;

		Json::Value header_json;
		try {
			header_json = string_to_json(header_string);
		}
		catch (...) {
		}

		if (header_json.isNull() || header_json.empty())
			return;

		uint32_t header_status;
		if (!header_json["status"].isNull() && !header_json["status"].empty())
			header_status = header_json["status"].asInt();

		std::string header_type;
		if (!header_json["type"].isNull() && !header_json["type"].empty())
			header_type = header_json["type"].asString();

		uint32_t header_size = 0;
		if (!header_json["size"].isNull() && !header_json["size"].empty())
			header_size = header_json["size"].asInt();

		std::string string_result = reader.get_current_string(header_size);

		//std::cout << "\n - - - - - - - - - - - - - START - - - - - - - - - - - - - - - - - - - - - ";

		//std::cout << "\n header_string: " << header_string;
		//std::cout << "\n size end: " << (size_header + header_size + 4);
		//std::cout << "\n string_result: " << string_result;

		if (rest_buffer.length() == (size_header + header_size + 4))
			rest_buffer = "";
		else
			rest_buffer = std::string(reader.get_current_data_pointer(), reader.bytes_left());

		this->set_response_string(string_result);

		//std::cout << "\n - - - - - - - - - - - - - END - - - - - - - - - - - - - - - - - - - - - \n";

		if (!rest_buffer.size())
			return;

		process_message(0, dados_no_buffer - (size_header + header_size));
	}

	static NavigationManager* get() {
		static NavigationManager* m = nullptr;
		if (!m)
			m = new NavigationManager();
		return m;
	}
};