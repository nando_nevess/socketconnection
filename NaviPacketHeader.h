#pragma once
#include <boost\asio.hpp>
#include <boost\bind.hpp>
#include <boost\thread.hpp>

#include <iostream>
#include <stdint.h>
#include <string>

#include <boost\chrono.hpp>
#include <functional>
#include <vector>
#include <memory>

#pragma pack(push,1)
class NaviPacketHeader{
public:
	uint16_t len = 0;
	uint32_t checksun;
	uint8_t protocol;
	uint8_t data[1];

	uint32_t get_checksun(){
		uint32_t a = 1, b = 0;
		size_t index;
		uint16_t sun_len = len - 6;

		for (index = 0; index < sun_len; ++index){
			a = (a + ((uint8_t*)&protocol)[index]) % 65521;
			b = (b + a) % 65521;
		}

		return (b << 16) | a;
	}
	void update_checksun(){
		checksun = get_checksun();
	}
	bool verify_checksun(){
		uint32_t new_checksun = get_checksun();
		return checksun == new_checksun;
	}
};
#pragma pack(pop)