#ifndef WPP_SOCKET_HPP
#define WPP_SOCKET_HPP

#include "socket_stream.h"
#include "socket_packet_header.h"
#include "socket_client.h"
#include "socket_scheduler.h"
#include "socket_crypto.h"

#include <iostream>

namespace wpp {
	namespace net {
		namespace socket {
			enum protocol_t {
				protocol_none,
				protocol_get_best_route,
				protocol_insert_user_path
			};

			struct message_server_info {
				uint8_t request_id = 0;
				std::string full_message = "";
				std::string message = "";
				uint32_t full_size = 0;
				bool complete = false;
			};

			class socket_client;

			struct rsa_key_struct {
				RSA* keypair;

				std::string string_public_key;
				char* public_key;
				int public_key_length;

				std::string string_private_key;
				char* private_key;
				int private_key_length;
			};

			class socket_manager : public socket_scheduler {
				wpp::net::socket::socket_crypto* crypto_socket;

				uint32_t client_from_server;
				uint32_t server_from_client;

				uint32_t size_rtt_client;
				uint32_t size_rtt_server;

				uint8_t key_rca = 0;

				bool compress_mode;
				bool rtt_mode;
				bool first_packet;

				bool defined_variables;
				bool defined_keys;

				unsigned char* remote_aes_key;
				unsigned char* remote_iv_key;

				wpp::thread::sync::mutex mtx_access;

				boost::asio::io_service svc;
				std::shared_ptr<socket_client> client_in;
				std::map<uint8_t, std::shared_ptr<message_server_info>> message_server_list;

				std::function<std::string(uint8_t, std::string)> callback_event_message_complete;

				socket_manager() : socket_scheduler(svc) {
					init();

					star_loop_client();

					std::thread([&]() { update_message_info(); }).detach();
				}

				std::shared_ptr<rsa_key_struct> local_rsa_key;
			public:
				void init() {
					crypto_socket = new wpp::net::socket::socket_crypto();

					client_from_server = 0;
					server_from_client = 0;
					remote_aes_key = NULL;

					compress_mode = false;
					first_packet = false;
					rtt_mode = false;

					defined_variables = false;
					defined_keys = false;

					local_rsa_key = generate_key();
				}

				std::shared_ptr<rsa_key_struct> generate_key() {
					int             ret = 0;
					RSA             *r = NULL;
					BIGNUM          *bne = NULL;
					BIO             *bp_public = NULL, *bp_private = NULL;

					int             bits = 2048;
					unsigned long   e = RSA_F4;

					// 1. generate rsa key
					bne = BN_new();
					ret = BN_set_word(bne, e);

					r = RSA_new();
					ret = RSA_generate_key_ex(r, bits, bne, NULL);

					BIO *pri = BIO_new(BIO_s_mem());
					ret = PEM_write_bio_RSAPrivateKey(pri, r, NULL, NULL, 0, 0, NULL);

					BIO *pub = BIO_new(BIO_s_mem());
					ret = PEM_write_bio_RSAPublicKey(pub, r);

					int pri_len = BIO_pending(pri);
					int pub_len = BIO_pending(pub);

					char * pri_key = (char*)calloc(pri_len, sizeof(char*));
					char * pub_key = (char*)calloc(pub_len, sizeof(char*));

					BIO_read(pri, pri_key, pri_len);
					BIO_read(pub, pub_key, pub_len);

					pri_key[pri_len] = '\0';
					pub_key[pub_len] = '\0';

					std::shared_ptr<rsa_key_struct> rsa_key = std::make_shared<rsa_key_struct>();

					rsa_key->keypair = r;
					rsa_key->public_key = pub_key;
					rsa_key->public_key_length = pub_len;
					rsa_key->string_public_key = std::string(pub_key,pub_len);

					rsa_key->private_key;
					rsa_key->private_key_length;
					rsa_key->string_private_key = std::string(pri_key,pri_len);

					return rsa_key;
				}

				void set_size_rtt_client(uint32_t in) { this->size_rtt_client = in; }
				void set_size_rtt_server(uint32_t in) { this->size_rtt_server = in; }
				void set_key_rca(uint8_t in) { this->key_rca = in; }
				void set_compress_mode(bool in) { this->compress_mode = in; }
				void set_rtt_mode(bool in){ this->rtt_mode = in; }

				uint32_t set_size_rtt_client() { return this->size_rtt_client; }
				uint32_t set_size_rtt_server() { return this->size_rtt_server; }
				uint8_t set_key_rca() { return this->key_rca; }
				bool set_compress_mode() { return this->compress_mode; }
				bool set_rtt_mode() { return this->rtt_mode; }

				void star_loop_client() {
					std::thread([&]() {
						client_in = std::shared_ptr<socket_client>(new socket_client(svc));

						client_in->set_callback_process_message([&](char* message, uint16_t size,uint8_t request_id) { this->process_message_packet_new(message, size, request_id); });
						client_in->set_callback_first_packet([&]() { this->send_first_packet(); });
						client_in->set_callback_process_first_packet([&](char* message, uint16_t size) { this->process_first_packet(); });

						while (true) {
							Sleep(10);
							try {
								svc.run();
							}
							catch (std::exception& ex) {
								std::cout << "\n Runner out reason: " << ex.what();
							}
						}
					}).detach();
				}
				
				void connect_to_server(std::string ip, uint32_t port) {
					if (!client_in)
						return;

					this->add_task(std::bind([](std::string _ip, uint16_t _port, std::shared_ptr<socket_client> _cli) -> void {
						_cli->set_connect(_ip, _port);

					}, ip, port, client_in), 2000);
				}
				void close() {
					client_in->close();
				}

				void set_callback_event_message_complete(std::function<std::string(uint8_t, std::string)> _callback) {
					this->callback_event_message_complete = _callback;
				}

				Json::Value string_to_json(std::string json_data) {
					Json::Value root;
					Json::Reader reader;
					bool parsingSuccessful = false;

					try {
						parsingSuccessful = reader.parse(json_data.c_str(), root);
					}
					catch (const std::exception&) {
						return Json::Value();
					}

					if (!parsingSuccessful)
						return Json::Value();

					return root;
				}
				std::string json_to_string(Json::Value json_string) {
					return json_string.toStyledString();
				}

				uint32_t string_to_uint(std::string in) {
					uint32_t temp = 0;
					try {
						temp = std::atoi(in.c_str());
					}
					catch (const std::exception&) {
						temp = 0;
					}
					return temp;
				}
			
				uint32_t swap_uint32(uint32_t val) {
					val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF);
					return (val << 16) | (val >> 16);
				}
				uint16_t swap_uint16(uint16_t val) {
					return (val << 8) | (val >> 8);
				}

				bool wait_server_result_complete(uint8_t request_id, uint32_t timeout) {
					wpp::time_ns::time_chronometer timer;
					while (timer.elapsed_milliseconds() < timeout) {
						if (!client_in->get_is_connect())
							return false;

						std::this_thread::sleep_for(std::chrono::milliseconds(10));

						mtx_access.lock();
						std::shared_ptr<message_server_info> current_message_server = message_server_list[request_id];
						if (current_message_server) {
							if (current_message_server->complete) {
								mtx_access.unlock();
								return true;
							}
						}
						mtx_access.unlock();
					}
					return false;
				}
				bool wait_result_first_packet(uint32_t timeout) {
					wpp::time_ns::time_chronometer timer;
					while (timer.elapsed_milliseconds() < timeout) {
						if (this->first_packet)
							return true;

						std::this_thread::sleep_for(std::chrono::milliseconds(10));
					}
					return false;
				}
				bool wait_defined_key(uint32_t time_out) {
					wpp::time_ns::time_chronometer timer;
					while (timer.elapsed_milliseconds() < time_out) {
						if (this->defined_keys)
							return true;

						std::this_thread::sleep_for(std::chrono::milliseconds(10));
					}
					return false;
				}
				
				std::string get_request_result(uint8_t request_id) {
					wpp::thread::sync::lock_guard <wpp::thread::sync::mutex>_guard(mtx_access);

					std::shared_ptr<message_server_info> current_server_message = message_server_list[request_id];
					if (!current_server_message)
						return "";

					if (!current_server_message->complete)
						return "";

					std::string message = current_server_message->message;

					current_server_message->full_message = "";
					current_server_message->message = "";
					current_server_message->complete = false;
					current_server_message->full_size = 0;

					return message;
				}
				
				std::string create_header(uint8_t current_request_id, std::string type, uint32_t size_result) {
					socket_packet_header header;
					header.size_header = type.size();

					std::string retval((char*)&header, sizeof(socket_packet_header));
					retval += type;

					return retval;
				}
				
				bool send_first_packet() {
					if (!client_in->get_is_connect() || client_in->get_connection_error())
						return false;

					std::string buffer_to_send;

					uint8_t temp_mode_rtt = rtt_mode ? 1 : 0;
					uint8_t temp_mode_compression = compress_mode ? 1 : 0;

					uint32_t temp_size_rtt_client = swap_uint32(size_rtt_client);
					uint32_t temp_size_rtt_server = swap_uint32(size_rtt_server);

					buffer_to_send.append((char*)&temp_mode_rtt, sizeof(uint8_t));
					buffer_to_send.append((char*)&temp_size_rtt_client, sizeof(uint32_t));
					buffer_to_send.append((char*)&temp_size_rtt_server, sizeof(uint32_t));
					buffer_to_send.append((char*)&temp_mode_compression, sizeof(uint8_t));
					
					if (!local_rsa_key)
						return false;

					std::string public_key = local_rsa_key->string_public_key;
					uint16_t public_key_length = swap_uint16(local_rsa_key->public_key_length);

					buffer_to_send.append((char*)&public_key_length, sizeof(uint16_t));
					buffer_to_send.append(public_key.data(), local_rsa_key->public_key_length);
															
					std::cout << "\n\n FIRST PACKET: " << buffer_to_send;

					this->defined_keys = false;
					this->defined_variables = false;

					std::shared_ptr<std::string> _buffer(new std::string(buffer_to_send.data(), buffer_to_send.size()));
					client_in->add_send_data(_buffer);

					return true;
				}
				bool send_request(uint8_t current_request_id, protocol_t protoccol_, Json::Value json_to_send) {
					if (!client_in->get_is_connect() || client_in->get_connection_error())
						return false;
					
					Json::Value json_to_send_;

					json_to_send["request_tst"] = std::to_string(std::chrono::milliseconds(time(NULL)).count());
					if (protoccol_ == protocol_get_best_route)
						json_to_send_["request_key"] = "get_best_route";
					else if (protoccol_ == protocol_insert_user_path)
						json_to_send_["request_key"] = "insert_user_path";

					//json_to_send_["request_key"] = "echo";
					//json_to_send_["params"] = "sdfasdfasdfasdfa";
					json_to_send_["params"] = json_to_send;

					if (json_to_send_.isNull() || json_to_send_.empty())
						return false;

					std::string json_string_to_send = json_to_send_.toStyledString();		
					json_string_to_send = wpp::string::remove_new_line(json_string_to_send);
					json_string_to_send = wpp::string::remove_space(json_string_to_send);

					std::string string_to_send = create_header(current_request_id, "json", json_string_to_send.size());
					string_to_send += json_string_to_send;

					wait_defined_key(30 * 1000);
					//wait_result_first_packet(20 * 1000);
					
					if(compress_mode)
						string_to_send = wpp::compression::lzma::compress_string(string_to_send);

					uint32_t compress_buffer_size = swap_uint32(string_to_send.size());

					std::string buffer_compress((char*)&compress_buffer_size, sizeof(uint32_t));
					buffer_compress += string_to_send;

			    	client_in->add_send_buffer_to_list(buffer_compress, current_request_id);
					return true;
				}

				std::pair<std::string, uint8_t> get_and_remove_request_id(char* buffer, uint16_t buffer_size) {
					uint8_t request_id = *(uint8_t*)&buffer[0];
					std::string buffer_string(buffer, buffer_size);

					uint32_t current_pos = 0;
					if (server_from_client > 0) {
						while (true) {
							if ((int)current_pos >= (int)(buffer_size - 1))
								break;

							buffer_string.erase(buffer_string.begin() + current_pos);
							current_pos += this->server_from_client;
						}
					}
					else
						buffer_string.erase(buffer_string.begin() + current_pos);

					return std::make_pair(buffer_string, request_id);
				}

				bool append_buffer_to_string(uint8_t request_id, std::string buffer_string) {
					wpp::thread::sync::lock_guard <wpp::thread::sync::mutex>_guard(mtx_access);

					if (request_id >= 254 || request_id <= -1)
						return false;

					std::shared_ptr<message_server_info> current_message = message_server_list[request_id];

					if (!current_message)
						current_message = std::shared_ptr<message_server_info>(new message_server_info);

					current_message->full_message.append(buffer_string.data(), buffer_string.size());
					current_message->complete = false;
					current_message->full_size = 0;

					message_server_list[request_id] = current_message;
					return true;
				}

				void update_message_info() {
					while (true) {
						Sleep(10);

						for (auto current_message_info : message_server_list) {
							if (!current_message_info.second)
								continue;

							if (current_message_info.second->complete)
								continue;

							mtx_access.lock();
							std::string temp_buffer = current_message_info.second->full_message;
							uint8_t current_request_id = current_message_info.first;
							mtx_access.unlock();

							socket_stream_reader reader((uint8_t*)temp_buffer.data(), temp_buffer.size());
							if (!reader.can_read_bytes(4))
								continue;

							//4 BYTES SIZE ALL PACKET COMPRESS
							uint32_t packet_size = swap_uint32(reader.get<uint32_t>());
							if (!reader.can_read_bytes(packet_size))
								continue;

							std::string buffer_compress = std::string(reader.get_current_data_pointer(), packet_size);

							std::string buffer_full = wpp::compression::lzma::decompress_string(buffer_compress);
							if (buffer_full == "error")
								buffer_full = buffer_compress;

							//1 BYTE SIZE HEADER PACKET DECOMPRESS
							uint8_t header_size = *(uint8_t*)&buffer_full[0];
							buffer_full.erase(0, 1);

							std::cout << "\n\n packet_size: " << std::to_string(packet_size);
							std::cout << "\n header_size: " << std::to_string(header_size);
							std::cout << "\n buffer_full: " << buffer_full;

							std::string header_string(buffer_full.data(), header_size);
							std::string result_string(buffer_full.begin() + header_size, buffer_full.begin() + buffer_full.size());

							result_string = wpp::string::remove_aspas(result_string);
							
							mtx_access.lock();
							current_message_info.second->complete = true;
							current_message_info.second->message = result_string;
							current_message_info.second->full_size = packet_size;
							mtx_access.unlock();

							if (callback_event_message_complete)
								callback_event_message_complete(current_request_id, "");
						}
					}
				}
				
				std::string rest_buffer;
				void process_message_packet_new(char* buffer, uint16_t buffer_size, uint8_t request_id) {
					if (!buffer_size)
						return;

					if (buffer)
						rest_buffer.append(buffer, buffer_size);
					
					if (!defined_variables) {
						if (!process_first_packet())
							return;

						defined_variables = true;
						return;
					}

					if(!defined_keys){			
						if (!process_keys_packet())
							return;						

						defined_keys = true;
						return;
					}

					uint8_t local_request_id = request_id;
					int size_read = 0;

					socket_stream_reader reader((uint8_t*)rest_buffer.data(), rest_buffer.size());
					if (local_request_id == 0) {
						if (!reader.can_read_bytes(1))
							return;

						local_request_id = reader.get<uint8_t>();
						size_read += sizeof(uint8_t);
					}

					if (!reader.can_read_bytes(2))
						return;

					uint32_t length_packet = swap_uint32(reader.get<uint32_t>());
					if (length_packet <= 0)
						return;

					if (!reader.can_read_bytes(length_packet))
						return;

					char* crypt_packet = reader.get_data_pointer(length_packet);
					size_read += (4 + length_packet);
					
					std::string message_decrypt = descrypt_message((unsigned char*)crypt_packet, length_packet);
					if (message_decrypt == "")
						return;

					process_message_decrypt(request_id, message_decrypt);

					if (buffer_size == size_read)
						rest_buffer.clear();
					else
						rest_buffer = std::string(reader.get_current_data_pointer(), reader.bytes_left());
					
					if (!rest_buffer.size())
						return;

					process_message_packet_new(0, buffer_size - size_read, local_request_id);
				}
			
				std::string descrypt_message(unsigned char* crypt_packet, int length_packet) {
					unsigned char derypted[2048] = {};

					int decrypt_length = crypto_socket->aesDecrypt2(crypt_packet, length_packet, remote_aes_key, (unsigned char*)derypted);
					if (decrypt_length <= 0)
						return "";

					return std::string((char*)derypted, decrypt_length);
				}
				void process_message_decrypt(int request_id, std::string buffer) {
					std::string packet_string = buffer;
					uint8_t header_size = (uint8_t)packet_string[0];

					packet_string.erase(0, 1);

					std::string header_string = std::string(packet_string.data(), header_size);
					std::string result_string = std::string(packet_string.begin() + header_size, packet_string.begin() + packet_string.size());

					std::shared_ptr<message_server_info> current_message = message_server_list[request_id];

					if (!current_message)
						current_message = std::shared_ptr<message_server_info>(new message_server_info);

					mtx_access.lock();
					current_message->complete = true;
					current_message->message = result_string;
					current_message->full_size = buffer.length();
					current_message->full_message = buffer;

					message_server_list[request_id] = current_message;
					mtx_access.unlock();
				}


				void process_message_packet(char* buffer, uint16_t buffer_size) {
					if (!buffer_size)
						return;

					if (!buffer)
						return;

					/*
					*SEU MACACO DO IFERNO
					*A PORRA DO CODIGO TA FUNCIONANDO S� QUE...
					*/

					if (!temp_first) {
						socket_stream_reader reader((uint8_t*)buffer, buffer_size);
						if (!reader.can_read_bytes(10))
							return;

						uint16_t length_rsa_public_key = swap_uint16(reader.get<uint16_t>());
						if (!reader.can_read_bytes((uint32_t)length_rsa_public_key))
							return;

						char* rsa_public_key = reader.get_data_pointer(length_rsa_public_key);

						uint16_t length_aes_public_key = swap_uint16(reader.get<uint16_t>());
						if (!reader.can_read_bytes(length_aes_public_key))
							return;

						char* aes_public_key = reader.get_data_pointer(length_aes_public_key);


						
	
						/*
						*	ESSA MERDA DE FUN��O get_current_string QUANDO ENCONTRA UM BYTE 0 PARA DE LER!, 
						*	PODE DEBUGAR E COMPARAR O VALOR COM O BUFFER NO SERVIDOR,
						*	SE VC RESOLVER ISSO, A VARIAVEL derypted VAI FICAR CORRETA

							O SERVIDOR E O SEU PROGRAMA EST�O PARADOS, BASTA COMPARAR A VARIAVEL LOCAL encryptedMessage e a variavel no servidor encAESKey
							se vc jogar o valor de encryptedKey dentro dos sites abertos no google chrome (ja fiz isso � so abrir) vai ver que ele leu ate o byte 215,
							se vc olhar no servidor, o byte 216 � um 0, corrige ai animal
						*
						*	S�O 4:14 DA MANH�
						*/
						
						RSA* local_keypair = local_rsa_key->keypair;

						BIO *pri = BIO_new(BIO_s_mem());
						PEM_write_bio_RSAPrivateKey(pri, local_keypair, NULL, NULL, 0, 0, NULL);

						BIO *pub = BIO_new(BIO_s_mem());
						PEM_write_bio_RSAPublicKey(pub, local_keypair);

						int pri_len = BIO_pending(pri);
						int pub_len = BIO_pending(pub);

						char * private_key_new = (char*)calloc(pri_len, sizeof(char*));
						char * public_key_new = (char*)calloc(pub_len, sizeof(char*));
						
						BIO_read(pri, private_key_new, pri_len);
						BIO_read(pub, public_key_new, pub_len);

						private_key_new[pri_len] = '\0';
						public_key_new[pub_len] = '\0';
						
						std::string private_key_old = local_rsa_key->string_private_key;
						std::string public_key_old = local_rsa_key->string_public_key;

						unsigned char derypted[2048] = {};

						int decrypted_length = RSA_private_decrypt(length_aes_public_key, (unsigned char*)aes_public_key, derypted, local_keypair, RSA_NO_PADDING);

						std::cout << "\n\n\n";
						for (auto current : derypted)
							std::cout << std::to_string((int)current) << " ";						
												 
						//std::string temp___ = (char*)decrypt_rsa_key;

						char * dec = (char *)derypted;
						
						ERR_load_crypto_strings();
						char * err = ERR_error_string(ERR_get_error(), NULL);
						fprintf(stderr, "%s\n", err);

						temp_first = true;
					}
					else {
						std::pair<std::string, uint8_t> buffer_pair_string_request_id = get_and_remove_request_id(buffer, buffer_size);
						append_buffer_to_string(buffer_pair_string_request_id.second, buffer_pair_string_request_id.first);
					}
				}
				





				bool process_first_packet() {
					wpp::thread::sync::lock_guard <wpp::thread::sync::mutex>_guard(mtx_access);
					message_server_list.clear();

					socket_stream_reader reader((uint8_t*)rest_buffer.data(), rest_buffer.size());
					if (!reader.can_read_bytes(9))
						return false;

					uint32_t size_header = reader.get<uint8_t>();

					client_from_server = swap_uint32(reader.get<uint32_t>());
					server_from_client = swap_uint32(reader.get<uint32_t>());

					client_in->set_client_from_server(client_from_server);

					rest_buffer = std::string(reader.get_current_data_pointer(), reader.bytes_left());
					return true;
				}
				bool process_keys_packet() {
					socket_stream_reader reader((uint8_t*)rest_buffer.data(), rest_buffer.size());
					if (!reader.can_read_bytes(2))
						return false;

					uint16_t length_rsa_public_key = swap_uint16(reader.get<uint16_t>());
					if (!reader.can_read_bytes((uint32_t)length_rsa_public_key))
						return false;

					char* rsa_public_key = reader.get_data_pointer(length_rsa_public_key);
					delete[] rsa_public_key;

					uint16_t length_aes_public_key = swap_uint16(reader.get<uint16_t>());
					if (!reader.can_read_bytes(length_aes_public_key))
						return false;

					char* aes_public_key = reader.get_data_pointer(length_aes_public_key);

					uint16_t length_iv_public_key = swap_uint16(reader.get<uint16_t>());
					if (!reader.can_read_bytes(length_iv_public_key))
						return false;

					char* iv_public_key = reader.get_data_pointer(length_iv_public_key);
					
					unsigned char derypted[2048] = {};

					RSA* local_keypair = local_rsa_key->keypair;
					int decrypted_length = RSA_private_decrypt(length_aes_public_key, (unsigned char*)aes_public_key, derypted, local_keypair, RSA_PKCS1_PADDING);
					if (!decrypted_length)
						return false;

					delete[] aes_public_key;

					this->remote_aes_key = derypted;
					this->remote_iv_key = (unsigned char*)iv_public_key;
					rest_buffer = std::string(reader.get_current_data_pointer(), reader.bytes_left());
					return true;
				}

				static socket_manager* get() {
					static socket_manager* m = nullptr;
					if (!m)
						m = new socket_manager();
					return m;
				}
			};
		}
	}
}
#endif