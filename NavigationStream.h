#pragma once
#include <boost\asio.hpp>
#include <boost\bind.hpp>
#include <boost\thread.hpp>

#include <iostream>
#include <stdint.h>
#include <string>

#include <boost\chrono.hpp>
#include <functional>
#include <vector>
#include <memory>

class NaviStreamWriter {
	std::string buffer;

public:
	void add_string(std::string str, bool set_size = true) {
		uint16_t len = static_cast<uint16_t>(str.length());
		if (set_size)
			buffer.append((char*)&len, sizeof(uint16_t));
		buffer.append(str.data(), str.size());
	}

	std::string get_data() {
		return buffer;
	}

	void allocate(uint32_t size) {
		std::string space;
		space.resize(size);
		buffer.append(space.data(), space.size());
	}

	template<typename T>
	void add(T value) {
		buffer.append((char*)&value, sizeof(T));
	}

	void add_string_vector(std::vector<std::pair<uint32_t, std::string>> vect) {
		add<uint32_t>(vect.size());
		for (auto it : vect) {
			add(uint32_t(it.first));
			add_string(it.second);
		}
	}
};

class NaviStreamReader {
	uint8_t* data_ptr;
	uint32_t stream_size;
	uint32_t current_pos = 0;

public:
	NaviStreamReader(uint8_t* _data, uint32_t data_len) {
		data_ptr = _data;
		stream_size = data_len;
	}

	void skip(uint32_t bytes_count) {
		current_pos += bytes_count;
	}

	bool eof() {
		return current_pos >= stream_size;
	}

	char* get_current_data_pointer() {
		return (char*)&data_ptr[current_pos];
	}

	std::string get_current_string(uint32_t size) {
		if (!can_read_bytes(size))
			return "";

		std::string retval((char*)&data_ptr[current_pos], size);
		current_pos += size;
		return retval;
	}


	uint32_t bytes_left() {
		if (current_pos >= stream_size)
			return 0;

		return stream_size - current_pos;
	}

	bool can_read_bytes(uint32_t b) {
		return (current_pos + b) <= stream_size;
	}

	std::string get_string() {
		if (!can_read_bytes(sizeof(uint16_t)))
			return "";

		uint16_t str_len = get<uint16_t>();
		if (!can_read_bytes(str_len))
			return "";

		std::string retval((char*)&data_ptr[current_pos], str_len);
		current_pos += str_len;
		return retval;
	}

	template<typename T>
	T get() {
		if (!can_read_bytes(sizeof(T)))
			return T();

		T value = *(T*)&data_ptr[current_pos];
		current_pos += sizeof(T);
		return value;
	}

	std::vector<std::pair<uint32_t, std::string>> get_string_vector() {
		std::vector<std::pair<uint32_t, std::string>> vect;
		uint32_t sz = get<uint32_t>();
		for (uint32_t i = 0; i < sz; i++) {
			vect.push_back(std::make_pair(get<uint32_t>(), get_string()));
		}

		return vect;
	}
};